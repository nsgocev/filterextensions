﻿using System;
using System.Linq;
using HedgehogDevelopment.SitecoreCommon.Data.Items;
using HedgehogDevelopment.SitecoreProject.Tasks.Extensibility;
using HedgehogDevelopment.SitecoreProject.Tasks.Filters.Extensions.Common;

namespace HedgehogDevelopment.SitecoreProject.Tasks.Filters.Extensions
{
    public class LastWeek : ICanIncludeItem
    {
        /// <summary>
        /// Only take the item if it was updated in the past week
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="parsedItem"></param>
        /// <returns></returns>
        public bool IncludeItemInBuild(string parameters, IItem parsedItem)
        {
            return
                parsedItem.Fields.Any(field => field.Name == "__Updated" &&
                                               Utility.IsoDateToDateTime(field.Value, DateTime.MinValue).Date >=
                                               DateTime.Now.AddDays(-7).Date);

        }
    }
}