﻿using System;
using System.Linq;
using HedgehogDevelopment.SitecoreCommon.Data.Items;
using HedgehogDevelopment.SitecoreProject.Tasks.Extensibility;
using HedgehogDevelopment.SitecoreProject.Tasks.Filters.Extensions.Common;

namespace HedgehogDevelopment.SitecoreProject.Tasks.Filters.Extensions
{
    public class LastNHours : ICanIncludeItem
    {
        /// <summary>
        /// Only take the item if it was updated in the past N hours
        /// </summary>
        /// <param name="parameters">The hours</param>
        /// <param name="parsedItem"></param>
        /// <returns></returns>
        public bool IncludeItemInBuild(string parameters, IItem parsedItem)
        {

            int hours = Utility.GetInt(parameters, int.MinValue);

            return
                parsedItem.Fields.Any(field => field.Name == "__Updated" &&
                                               Utility.IsoDateToDateTime(field.Value, DateTime.MinValue) >=
                                               DateTime.Now.AddHours(-hours));
        }
    }
}