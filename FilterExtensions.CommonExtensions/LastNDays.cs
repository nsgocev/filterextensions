﻿using System;
using System.Linq;
using HedgehogDevelopment.SitecoreCommon.Data.Items;
using HedgehogDevelopment.SitecoreProject.Tasks.Extensibility;
using HedgehogDevelopment.SitecoreProject.Tasks.Filters.Extensions.Common;

namespace HedgehogDevelopment.SitecoreProject.Tasks.Filters.Extensions
{
    public class LastNDays : ICanIncludeItem
    {  
       /// <summary>
       /// Only take the item if it was updated in the past N days
       /// </summary>
       /// <param name="parameters">The days</param>
       /// <param name="parsedItem"></param>
       /// <returns></returns>
        public bool IncludeItemInBuild(string parameters, IItem parsedItem)
        {
            int days = Utility.GetInt(parameters, int.MinValue);

            if (days == 0)
            {
                return true;
            }

            return
                parsedItem.Fields.Any(field => field.Name == "__Updated" &&
                                               Utility.IsoDateToDateTime(field.Value, DateTime.MinValue).Date >=
                                               DateTime.Now.AddDays(-days).Date);

        }
    }
}