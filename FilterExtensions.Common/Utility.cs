﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace HedgehogDevelopment.SitecoreProject.Tasks.Filters.Extensions.Common
{
    public class Utility
    {
        public static DateTime IsoDateToDateTime(string isoDate, DateTime defaultValue)
        {
            if (isoDate != null)
            {
                isoDate = isoDate.Replace("Z", "");

                try
                {
                    if ((isoDate.Length > 15) && (isoDate[15] == ':'))
                    {
                        string str = isoDate.Substring(0x10);
                        if (str.Length > 0)
                        {
                            return new DateTime(GetLong(str, 0L));
                        }
                    }

                    int[] isoDateParts = GetIsoDateParts(isoDate);

                    if (isoDateParts == null)
                    {
                        return defaultValue;
                    }

                    if (isoDateParts.Length >= 6)
                    {
                        return new DateTime(isoDateParts[0], isoDateParts[1], isoDateParts[2], isoDateParts[3],
                            isoDateParts[4], isoDateParts[5]);
                    }

                    if (isoDateParts.Length < 3)
                    {
                        return defaultValue;
                    }

                    return new DateTime(isoDateParts[0], isoDateParts[1], isoDateParts[2]);
                }
                catch
                {

                }
            }

            return defaultValue;
        }

        public static T XmlDeserilize<T>(string xmlString) where T : class
        {
            if (!string.IsNullOrEmpty(xmlString))
            {

                XmlSerializer serializer = new XmlSerializer(typeof (T));

                using (TextReader reader = new StringReader(xmlString))
                {
                    return (T) serializer.Deserialize(reader);
                }

            }

            return null;
        }

        public static async Task<T> GetJsonResult<T>(string url) where T : class
        {
           return await GetJsonResult<T>(url,string.Empty,string.Empty);
        }

        public static async Task<T> GetJsonResult<T>(string url, string username, string password) where T: class
        {

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                    Convert.ToBase64String(
                        System.Text.Encoding.ASCII.GetBytes(
                            string.Format("{0}:{1}", username, password))));


                using (HttpResponseMessage response = client.GetAsync(
                    url).Result)
                {
                    response.EnsureSuccessStatusCode();
                    string responseString = await response.Content.ReadAsStringAsync();

                    if (!string.IsNullOrEmpty(responseString))
                    {
                        return JsonConvert.DeserializeObject<T>(responseString);
                    }
                }
            }

            return null;
        }

        public static long GetLong(string value, long defaultValue)
        {
            long result;

            if (string.IsNullOrEmpty(value) || !long.TryParse(value, out result))
            {
                return defaultValue;
            }

            return result;
        }

        public static int GetInt(string value, int defaultValue)
        {
            int result;

            if (string.IsNullOrEmpty(value) || !int.TryParse(value, out result))
            {
                return defaultValue;
            }

            return result;
        }

        private static int[] GetIsoDateParts(string isoDate)
        {
           
            if (isoDate.Length != 8 && isoDate.Length != 9 && isoDate.Length != 15 && isoDate.Length != 16)
            {
                return null;

            }

            if (Regex.IsMatch(isoDate, "[^0-9TZ]"))
            {
                return null;
            }

            int[] numArray =
            {
                GetInt(isoDate.Substring(0, 4), 0),
                GetInt(isoDate.Substring(4, 2), 0),
                GetInt(isoDate.Substring(6, 2), 0),
                0,
                0,
                0
            };

            if (isoDate.Length > 8 && isoDate[8] == 84)
            {
                numArray[3] = GetInt(isoDate.Substring(9, 2), 0);
                numArray[4] = GetInt(isoDate.Substring(11, 2), 0);
                numArray[5] = GetInt(isoDate.Substring(13, 2), 0);
            }

            
            return numArray;
        }
      
    }
}