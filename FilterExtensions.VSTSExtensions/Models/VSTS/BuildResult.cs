﻿using System.Collections.Generic;

namespace FilterExtensions.VSTSExtensions.Models.VSTS
{
    public class BuildResult
    {
        public int Count { get; set; }

        public IEnumerable<Build> Value { get; set; }
    }
}
