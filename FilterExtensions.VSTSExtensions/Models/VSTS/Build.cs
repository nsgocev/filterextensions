﻿using System;

namespace FilterExtensions.VSTSExtensions.Models.VSTS
{
    // we only need the queue time
    public class Build
    {
        public DateTime QueueTime { get; set; }
    }
}