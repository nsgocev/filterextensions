﻿namespace FilterExtensions.VSTSExtensions.Models.Parameters
{
    public class VSTSDetails
    {
        public string QueryUrl { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
