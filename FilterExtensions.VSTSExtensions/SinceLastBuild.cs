﻿using System;
using System.Linq;
using FilterExtensions.VSTSExtensions.Models.Parameters;
using FilterExtensions.VSTSExtensions.Models.VSTS;
using HedgehogDevelopment.SitecoreCommon.Data.Items;
using HedgehogDevelopment.SitecoreProject.Tasks.Extensibility;
using HedgehogDevelopment.SitecoreProject.Tasks.Filters.Extensions.Common;

namespace FilterExtensions.VSTSExtensions
{
    public class SinceLastBuild : ICanIncludeItem
    {
        private static VSTSDetails vstsDetails;
        private static BuildResult buildResult;

        public bool IncludeItemInBuild(string parameters, IItem parsedItem)
        {
            if (buildResult == null)
            {
                if (vstsDetails == null)
                {
                    vstsDetails = Utility.XmlDeserilize<VSTSDetails>(parameters);
                }


                if (vstsDetails != null && !string.IsNullOrEmpty(vstsDetails.QueryUrl))
                {
                    buildResult = Utility.GetJsonResult<BuildResult>(vstsDetails.QueryUrl, vstsDetails.Username,
                        vstsDetails.Password).Result;
                }
            }

            if (buildResult != null && buildResult.Count > 0)
            {
                return
                    parsedItem.Fields.Any(field => field.Name == "__Updated" &&
                                                   Utility.IsoDateToDateTime(field.Value, DateTime.MinValue).Date >=
                                                   buildResult.Value.First().QueueTime);
            }

            return false;
        }
    }
}
